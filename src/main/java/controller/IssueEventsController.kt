package main.java.controller

import main.java.model.IssueEvent
import main.java.service.IssueEventsService

/**
 * Controller da Entidade IssueEvent
 * @author juccelino.barros
 */
class IssueEventsController {

    private val service = IssueEventsService()

    /**
     * Lista todos os eventos de uma Issue especifica
     * Path GET issues/:number/unity
     */
    fun findByNumber(number:Int): List<IssueEvent> {
        return service.findByNumber(number)
    }

    /**
     * Insere o registro de um evento da Issue no BD a partir do webhook
     * Path POST /github-webhook
     */
    fun save(issueEvent: IssueEvent) {
        service.save(issueEvent)
    }
}