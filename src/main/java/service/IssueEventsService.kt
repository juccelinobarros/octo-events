package main.java.service

import main.java.dao.IssueEventsDao
import main.java.model.IssueEvent

/**
 * Camada de servico da entidade IssueEvents
 * @author juccelino.barros
 */
class IssueEventsService {

    private val dao = IssueEventsDao()

    /**
     * Retorna todos os eventos da Issue consultada
     */
    fun findByNumber(numberIssue:Int): List<IssueEvent> {
        return dao.findByNumber(numberIssue)
    }

    /**
     * Insere os eventos da Issue a partir do webhook
     */
    fun save(issueEvent: IssueEvent) {
        dao.save(issueEvent)
    }
}