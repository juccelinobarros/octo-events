package main.java

import io.javalin.Javalin
import main.java.controller.IssueEventsController
import main.java.model.IssueEvent
import main.java.util.ParserToIssueEvent
import org.jetbrains.exposed.sql.Database
import java.io.FileInputStream
import java.lang.NumberFormatException
import java.util.*

/**
 * Classe que executa o servidor e a conexao com bd
 * @author juccelino.barros
 */
class ServerInstance {
    fun main () {

        /**
         * Controllers da aplicacao
         */
        val controller = IssueEventsController()

        /**
         * Javalin Server
         */
        val app = Javalin.create().apply {
            exception(Exception::class.java) { e, ctx -> e.printStackTrace() }
            error(404) { ctx -> ctx.json("not found") }
        }.start(7000)

        app.routes {

            /**
             * GET Consulta no Banco de Dados por numero da Issue
             */
            app.get( "/issues/:numberissue/events") { ctx ->
                ctx.json(controller.findByNumber(ctx.pathParam("numberissue").toInt()))
                ctx.status(200)
            }.exception(NumberFormatException::class.java) {
                    _, ctx -> ctx.json("Bad Request!").status(400)
            }

            /**
             * POST do Webhook Github (Issues) - Projeto: UserServer
             *
             * https://github.com/jukabarros/userServer/issues
             * Insere no Banco de Dados local
             */
            app.post("github-webhook") {ctx ->
                val issueEvent : IssueEvent? = ParserToIssueEvent().getIssueEventJson(ctx.body())
                if(null != issueEvent) {
                    controller.save(issueEvent)
                    ctx.status(200)
                }
            }.exception(NullPointerException::class.java) {
                    _, ctx -> ctx.json("Bad Request!").status(400)
            }
        }


        /**
         * Informacoes de Conexao com o Banco de Dados
         */
        val propertiesFile = System.getProperty("user.dir") + "/src/main/resources/application.properties"
        val fis = FileInputStream(propertiesFile)
        val configFile = Properties()
        configFile.load(fis)

        Database.connect("jdbc:postgresql://"+configFile.getProperty("db.host")+":5432/"+configFile.getProperty("db.name"),
            driver = "org.postgresql.Driver",
            user = configFile.getProperty("db.user"), password = configFile.getProperty("db.password"))

    }
}
