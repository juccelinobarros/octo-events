package main.java

/**
 * Classe que instancia o servidor e a conexao com o BD
 * @author juccelino.barros
 */
fun main(args: Array<String>) {
    ServerInstance().main()
}