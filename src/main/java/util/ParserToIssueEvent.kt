package main.java.util

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import main.java.model.IssueEvent
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

/**
 * Converte o JSON para o Objeto IssueEvents
 * @author juccelino.barros
 */
class ParserToIssueEvent {

    private var formatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ssZZ")

    /**
     * Converte json do webhook para o objeto IssueEvent
     */
    fun getIssueEventJson(json: String): IssueEvent? {
        val objMapper = ObjectMapper()
        val fullJson: JsonNode = objMapper.readTree(json)

        val action = fullJson["action"].textValue()

        // Varrer os atributos de Issue
        val issueInfo = fullJson.path("issue")
        val numberIssue = issueInfo["number"].asInt()
        val authorAssociation: String? = issueInfo["author_association"].asText()

        val createdAtStr: String? = issueInfo["created_at"].asText()
        val createdAtDate = convertToDateTime(createdAtStr)

        val updateAtStr: String? = issueInfo["updated_at"].asText()
        val updatedAtDate = convertToDateTime(updateAtStr)

        val closedAtStr: String? = issueInfo["closed_at"].asText()
        val closedAtDate = convertToDateTime(closedAtStr)

        // id null --> auto_increment no bd
        return IssueEvent(
            id = null, numberIssue = numberIssue, action = action, authorAssociation = authorAssociation,
            createdAt = createdAtDate, updatedAt = updatedAtDate, closedAt = closedAtDate
        )

    }

    /**
     * Converte os campos datas (string) para datetime (jodatime)
     */
    private fun convertToDateTime(dateStr: String?): DateTime? {
        if (dateStr.equals("null") || dateStr.isNullOrEmpty()) {
            return null
        }
        return formatter.parseDateTime(dateStr)
    }
}