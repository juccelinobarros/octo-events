package main.java.util

import com.fasterxml.jackson.core.JsonProcessingException
import java.io.IOException
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonSerializer
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

/**
 * Classe resopnsavel por transformar os atributos JodaDateTime
 * em String para fins de visualizacao no JSON
 *
 * @author juccelino.barros
 */
class CustomDateSerializer : JsonSerializer<DateTime>() {

    @Throws(IOException::class, JsonProcessingException::class)
    override fun serialize(value: DateTime, gen: JsonGenerator, arg2: SerializerProvider) {
        gen.writeString(formatter.print(value))
    }

    companion object {
        private val formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
    }
}