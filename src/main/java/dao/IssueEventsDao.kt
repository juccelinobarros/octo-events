package main.java.dao

import main.java.model.IssueEvent
import main.java.model.IssueEvents
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction

/**
 * Camada DAO da entidade IssueEvents
 * @author juccelino.barros
 */
class IssueEventsDao {

    /**
     * Consulta todos eventos referente a Issue (Number)
     * @return List<IssueEvent>
     *
     */
    fun findByNumber(numberIssue:Int): List<IssueEvent> {
        var allIssueEvents = listOf<IssueEvent>()
        transaction {
            allIssueEvents = IssueEvents.select { IssueEvents.numberIssue eq numberIssue }.map {
                IssueEvent(it[IssueEvents.id], it[IssueEvents.numberIssue],
                    it[IssueEvents.action], it[IssueEvents.authorAssociation], it[IssueEvents.createdAt]
                    , it[IssueEvents.updatedAt], it[IssueEvents.closedAt])
            }
        }
        return allIssueEvents
    }

    /**
     * Insere o registro no banco de dados quando
     * o Webhook envia as informacoes
     */
    fun save(issueEvent:IssueEvent) {
        transaction {
            IssueEvents.insert {
                it[numberIssue] = issueEvent.numberIssue
                it[action] = issueEvent.action
                it[authorAssociation] = issueEvent.authorAssociation
                it[createdAt] = issueEvent.createdAt
                it[updatedAt] = issueEvent.updatedAt
                it[closedAt] = issueEvent.closedAt
            }
        }
    }

}
