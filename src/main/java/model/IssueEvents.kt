package main.java.model

import com.fasterxml.jackson.databind.annotation.JsonSerialize
import main.java.util.CustomDateSerializer
import org.jetbrains.exposed.sql.Table
import org.joda.time.DateTime

/**
 * Entidade IssueEvent
 * @author juccelino.barros
 */

object IssueEvents : Table(name = "issue_events") {
    val id = integer("id").primaryKey()
    val numberIssue = integer("num_issue")
    val action = varchar("act", 50)
    val authorAssociation = varchar("author_association", 100).nullable()
    val createdAt = datetime("created_at").nullable()
    val updatedAt = datetime("updated_at").nullable()
    val closedAt = datetime("closed_at").nullable()
}

data class IssueEvent(
    val id: Int?,
    val numberIssue: Int,
    val action: String,
    val authorAssociation: String?,
    @get:JsonSerialize(using = CustomDateSerializer::class) val createdAt: DateTime?,
    @get:JsonSerialize(using = CustomDateSerializer::class) val updatedAt: DateTime?,
    @get:JsonSerialize(using = CustomDateSerializer::class) val closedAt: DateTime?

)
