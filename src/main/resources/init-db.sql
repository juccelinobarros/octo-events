-- Criar via command line (terminal)
create database octoevents_db;

CREATE TABLE public.issue_events (
	id serial NOT NULL,
	num_issue int4 NOT NULL,
	act varchar(50) NOT NULL,
	author_association varchar(100) NULL,
	created_at timestamp NULL,
	updated_at timestamp NULL,
	closed_at timestamp NULL,
	CONSTRAINT issue_events_pkey PRIMARY KEY (id)
);

--- Inserir Esses registros para validacao dos testes unitarios
INSERT INTO public.issue_events (num_issue,act,author_association, created_at) VALUES
(1,'open', 'juka', '2019-03-22 19:17:32.490')
,(1,'closed', 'juka', '2019-03-22 19:17:32.746')
,(2,'open', 'juka', '2019-03-22 19:17:32.748')
,(2,'closed','juka', '2019-03-22 19:17:32.751');