package config

import io.restassured.RestAssured
import main.java.ServerInstance
import org.junit.BeforeClass

/**
 * Classe que realiza a configuracao do RestAssured
 * Instancia o servidor antes da realizacao dos testes
 *
 * @author juccelino.barros
 */
open class TestConfig {

    companion object {
        private var serverStarted = false

        @BeforeClass
        @JvmStatic
        fun startServer() {
            if(!serverStarted) {
                ServerInstance().main()
                serverStarted = true
                RestAssured.port = 7000
                RestAssured.baseURI = "http://localhost"
            }
        }
    }

}