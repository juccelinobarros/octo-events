package unity

import config.TestConfig
import io.restassured.RestAssured
import org.hamcrest.Matchers
import org.junit.Test

/**
 * Test Path GET issues/:number/events
 *
 * @author juccelino.barros
 */
class IssuesEventsTest: TestConfig() {

    private val path: String = "/issues/"
    private val numberOK: Int = 1
    private val numberNotFound: Int = 999999
    private val numberInvalid: String = "invalidNumber"

    /**
     * Realiza a consulta dos eventos da Issue cadastrada
     *
     * Retorno esperado: 200
     */
    @Test
    fun testGetEventsByNumberOK() {
        RestAssured
            .given()
            .get("${path+numberOK}/events")
            .then()
            .statusCode(200)
            .body("size()", Matchers.greaterThan(0))
    }

    /**
     * Realiza a consulta dos eventos da Issue que nao existe
     *
     * Retorno esperado: 200, porem com a lista vazia
     */
    @Test
    fun testGetEventsByNumberNotFound() {
        RestAssured
            .given()
            .get("${path+numberNotFound}/events")
            .then()
            .statusCode(200)
            .body("size()", Matchers.equalTo(0))
    }

    /**
     * Realiza a consulta dos eventos da Issue de um numero invalido (text)
     *
     * Retorno esperado: 400
     */
    @Test
    fun testGetEventsByNumberInvalid() {
        RestAssured
            .given()
            .get("${path+numberInvalid}/events")
            .then()
            .statusCode(400)
    }

}