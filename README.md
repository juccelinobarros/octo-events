**OCTO-EVENTS**

**Descrição:**

Aplicação responsável por monitorar e registar os eventos nas _Issues_ do projeto UserServer,
o qual está hospedado no Github. As informações das _Issues_ são obtidas através do Webhook.

_Link UserServer: https://github.com/jukabarros/userServer/_

**Ferramentas e Tecnologias**

Banco de Dados: Exposed (Database/Datasource: Postgres 9.6)

Linguagem de Programação: Kotlin

Frameworks Kotlin: Javalin

Testes: JUnit e RestAssured

Gerenciador de Dependências: Maven

**Primeiros Passos**

1 - Importar Projeto Maven

2 - Criar banco de dados: octoevents_db (via command line - terminal)

3 - Criar tabela: issue_events (ver script em main/resources/init-db.sql)

CREATE TABLE public.issue_events (
	id serial NOT NULL,
	num_issue int4 NOT NULL,
	act varchar(50) NOT NULL,
	author_association varchar(100) NULL,
	created_at timestamp NULL,
	updated_at timestamp NULL,
	closed_at timestamp NULL,
	CONSTRAINT issue_events_pkey PRIMARY KEY (id)
);

3.1 - Inserir os seguintes registros para validação dos testes unitários

INSERT INTO public.issue_events (num_issue,act,author_association, created_at) VALUES

(1,'open', 'juka', '2019-03-22 19:17:32.490')

,(1,'closed', 'juka', '2019-03-22 19:17:32.746')

,(2,'open', 'juka', '2019-03-22 19:17:32.748')

,(2,'closed','juka', '2019-03-22 19:17:32.751');

4 - Configurar os dados de acesso ao banco de dados em _application.properties_

5 - mvn install

**Paths**

1 - GET _"/issues/:numberissue/events"_

Retorna todos os eventos da issue consultada. As informações são provenientes do Banco de dados da aplicação.
Parâmetro: _numberissue - INT_


2 - POST _"/github-webhook"_

Cadastra o evento da issue(1) do projeto "UserServer" no banco de dados local. Recomendável utilizar o ngrok(2) para
configurar o webhook como também acompanhar as requests HTTP (POST) enviadas para aplicação.

(1) https://developer.github.com/v3/activity/events/types/#issuesevent

(2) https://ngrok.com/

**Informações Adicionais**

Porta padrão da aplicação: _7000_

Arquivo de Configuração: _application.properties_

**Erros Conhecidos**

_1 - Conexão com o Banco de Dados_

Problema ao tentar ler as informações no arquivo properties. Neste caso, deve-se
inserir manualmente as credenciais do banco de dados no arquivo ServerInstance, linha 70. Além disso, deve-se comentar
as linhas 65 a 68 do mesmo arquivo.

_2 - Testes RestAssured_

Verificar se as portas do servidor (linha 30 - ServerInsance.kt) e do restAssured (linha 24 - TestConfig.kt)
são iguais.